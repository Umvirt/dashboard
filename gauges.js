function drawGauge(obj){
var x=obj.position.x;
var y=obj.position.y;
//half
var halfsize=obj.size/2;

ctx.lineWidth = halfsize	* 0.005;

var path = new Path2D();
//draw circle #1
path.arc(x,y,halfsize,0,360);
//add gradient
ctx.fillStyle = '#eee'
ctx.fill(path);
ctx.stroke(path);
//draw circle #2
path = new Path2D();
path.arc(x,y,halfsize*0.95,0,360);
//add gradient
var gradient = ctx.createLinearGradient(x, y-halfsize, x, y-halfsize+halfsize*0.8);
gradient.addColorStop(0, '#555');
gradient.addColorStop(1, '#000');
ctx.fillStyle = gradient;
ctx.fill(path);
ctx.stroke(path);
//add gauge label
ctx.font = (halfsize*0.07)+"px Helvetica";
ctx.strokeStyle = '#fff'
ctx.fillStyle = '#fff'
if(obj.label1){
var r=halfsize*0.4
P3=[r*Math.cos(-2)+x,r*Math.sin(-2)+y]
ctx.fillText(obj.label1, P3[0]-10, P3[1]);
}
if(obj.label2){
var r=halfsize*0.25
P3=[r*Math.cos(-1.8)+x,r*Math.sin(-1.8)+y]
ctx.fillText(obj.label2, P3[0]-10, P3[1]);
}
//clear dataset
data[obj.id]=[];
//console.log(data[obj.id]);
var xdata=data[obj.id];
//for each segment
for(var  i=0;i<obj.segments.length;i++){
	maxi=0;
	if(i<obj.segments.length-1){
		maxi=obj.segments[i+1].label-0.001
	}
	//append data
    //console.log({segment: obj.segments[i], min: obj.segments[i].label, max:maxi})
	//console.log(segments[i].label)
	xdata.push({segment: obj.segments[i], min: obj.segments[i].label, max:maxi})
	}
//console.log(data);
//return;
//scale begining
var startAngle=-3.5
//scale ending
var endAngle=0.8

//starting
var currentAngle = startAngle;

//segments start debug
/*
var path = new Path2D();
path.moveTo(halfsize,halfsize);
var r=halfsize*0.8
P1=[r*Math.cos(currentAngle)+halfsize,r*Math.sin(currentAngle)+halfsize]
path.lineTo(P1[0],P1[1]);
ctx.strokeStyle = '#fff'
ctx.stroke(path);
*/

//segments end debug
/*
var path = new Path2D();
path.moveTo(halfsize,halfsize);
var r=halfsize*0.8
P1=[r*Math.cos(endAngle)+halfsize,r*Math.sin(endAngle)+halfsize]
path.lineTo(P1[0],P1[1]);
ctx.strokeStyle = '#fff'
ctx.stroke(path);
*/


//segments margins calcuation and drawing 
currentAngle=startAngle
for(item in xdata){
	//console.log(endAngle-startAngle)
	//console.log(1 / data.length)
	//console.log(endAngle-startAngle* 2 * Math.PI*(1 / data.length))
	portionAngle = Math.abs(endAngle-startAngle) *(1 / xdata.length);
	xdata[item].startAngle=currentAngle;
	xdata[item].endAngle=currentAngle+portionAngle;
	//draw start margin	
	/*var path = new Path2D();
	ctx.strokeStyle = '#fff'
	ctx.lineWidth = halfsize	* 0.025;
	var r=halfsize*0.625
	P1=[r*Math.cos(currentAngle)+halfsize,r*Math.sin(currentAngle)+halfsize]
	var r=halfsize*0.55
	P2=[r*Math.cos(currentAngle)+halfsize,r*Math.sin(currentAngle)+halfsize]
	path.moveTo(P2[0],P2[1]);
	path.lineTo(P1[0],P1[1]);
	ctx.stroke(path);
	*/
	//draw label
	//console.log(currentAngle);
	ctx.font = (halfsize*0.14)+"px Helvetica";
	ctx.strokeStyle = '#fff'
	ctx.fillStyle = '#fff'
	var r=halfsize*0.75
	P3=[r*Math.cos(currentAngle)+x,r*Math.sin(currentAngle)+y]
	var xshift=halfsize	* 0.1
	ctx.fillText(xdata[item].segment.label, P3[0]-xshift, P3[1]);

	//fill segment
	if(xdata[item].segment.color){
	var r=halfsize*0.575
	P4=[r*Math.cos(currentAngle+portionAngle)+x,r*Math.sin(currentAngle+portionAngle)+y]
	var path = new Path2D();
	ctx.strokeStyle = xdata[item].segment.color;
	ctx.lineWidth = halfsize * 0.05;
	path.arc(x,y,r,currentAngle+0.01,currentAngle+portionAngle-0.01);
	ctx.stroke(path);
	}

	//draw start margin	
	var path = new Path2D();
	ctx.strokeStyle = '#fff'
	ctx.lineWidth = halfsize	* 0.025;
	var r=halfsize*0.625
	P1=[r*Math.cos(currentAngle)+x,r*Math.sin(currentAngle)+y]
	var r=halfsize*0.55
	P2=[r*Math.cos(currentAngle)+x,r*Math.sin(currentAngle)+y]
	path.moveTo(P2[0],P2[1]);
	path.lineTo(P1[0],P1[1]);
	ctx.stroke(path);
	

	currentAngle=currentAngle+portionAngle;
}

//value pointer drawing 
drawValue(obj,values[obj.id])

//draw pointer cover
//draw circle #1
var path = new Path2D();
ctx.lineWidth = 1;
ctx.strokeStyle = '#000';
path.arc(x,y,halfsize*0.175,0,360);
ctx.fillStyle = '#333'
ctx.fill(path);
ctx.stroke(path);
path = new Path2D();
path.arc(x,y,halfsize*0.15,0,360);
//ctx.fillStyle = '#000'
var gradient = ctx.createLinearGradient(x, y-halfsize*0.15, x, y+halfsize*0.15*1.22);

// Add three color stops
gradient.addColorStop(0, '#444');
gradient.addColorStop(1, '#000');

ctx.fillStyle = gradient;

ctx.fill(path);
}

function drawValue(obj,val){
var xdata=data[obj.id];
//console.log(data);

var x=obj.position.x;
var y=obj.position.y;
//half
var halfsize=obj.size/2;
//radius
var r=0;

var segment=null;
//segment selection
for(item in xdata){
if((xdata[item].max>val) && (xdata[item].min<=val)){
segment=xdata[item];
}
if((xdata[item].max==0) && (xdata[item].min<=val)){
segment=xdata[item];
}
}

//if segment selected
if(segment){
//debug
//console.log(val+'->'+segment.min);

if(segment.max){
d1=segment.max-segment.min
d2=segment.max-val
r=d2/d1
}else{
r=0.5
}

a1=segment.endAngle-segment.startAngle
angle=segment.endAngle-a1*r
ctx.lineWidth = halfsize*0.03;


var path = new Path2D();
path.moveTo(x,y);
r=0.65*halfsize;
P1=[r*Math.cos(angle)+x,r*Math.sin(angle)+y]
path.lineTo(P1[0],P1[1]);
ctx.strokeStyle = '#f00';
ctx.stroke(path);

//console.log(r)

}




}

