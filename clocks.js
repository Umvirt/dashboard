function drawClock(obj){
//half
var halfsize=obj.size/2;

var x=obj.position.x;
var y=obj.position.y;


//init
data['obj.id']={}
data['obj.id']['scale60']=[];
var data2=data['obj.id']['scale60']
data['obj.id']['scale12']=[];
var data12=data['obj.id']['scale12']

ctx.lineWidth = halfsize	* 0.005;

//draw circle #1
var path = new Path2D();
path.arc(x,y,halfsize,0,360);
ctx.fillStyle = '#eee'
ctx.fill(path);
ctx.stroke(path);

//draw circle #2
path = new Path2D();
path.arc(x,y,halfsize*0.95,0,360);
ctx.stroke(path);
//add gradient
var gradient = ctx.createLinearGradient(x, y-halfsize, x, y-halfsize+halfsize*0.8);
gradient.addColorStop(0, '#555');
gradient.addColorStop(1, '#000');
ctx.fillStyle = gradient;
ctx.fill(path);

//create 12 intervals scale
for(var i=1;i<=12;i++){
//console.log(i);
data12.push({label:i});
}
//var path = new Path2D();

var portionAngle = (1 / data12.length) * 2 * Math.PI;
var currentAngle = -(Math.PI/2)+portionAngle;

for(item in data12){
portionAngle = (1 / data12.length) * 2 * Math.PI;
data12[item].angle=currentAngle;
var path = new Path2D();
path.moveTo(x,y);
var r=halfsize*0.8
P1=[r*Math.cos(currentAngle)+x,r*Math.sin(currentAngle)+y]
path.moveTo(P1[0],P1[1]);
ctx.strokeStyle = '#fff'
ctx.lineWidth = halfsize	* 0.015;
ctx.stroke(path);
currentAngle=currentAngle+portionAngle;
//console.log(currentAngle);
ctx.font = halfsize*0.25+"px Helvetica";
var xshift=-0.075*halfsize;
var yshift=0.075*halfsize;
ctx.fillStyle = '#fff'
ctx.fillText(data12[item].label, P1[0]+xshift, P1[1]+yshift);

}
//seconds

data2=[];

for(var i=1;i<=60;i++){
//console.log(i);
if(i<60){
data2.push({label:i});
}else{
data2.push({label:0});
}
}

//var path = new Path2D();

var portionAngle = (1 / data2.length) * 2 * Math.PI;
var currentAngle = -(Math.PI/2)+portionAngle;


for(item in data2){
portionAngle = (1 / data2.length) * 2 * Math.PI;

var path = new Path2D();
path.moveTo(x,y);
var r=halfsize*0.55
P1=[r*Math.cos(currentAngle)+x,r*Math.sin(currentAngle)+y]
if(data2[item].label % 5 ==0){
var r=halfsize*0.7
}else{
var r=halfsize*0.6
}

P2=[r*Math.cos(currentAngle)+x,r*Math.sin(currentAngle)+y]

path.moveTo(P1[0],P1[1]);
path.lineTo(P2[0],P2[1]);

data2[item].angle=currentAngle;

ctx.stroke(path);
currentAngle=currentAngle+portionAngle;
//console.log(currentAngle);
//debug
//ctx.font = "10px Helvetica";
//ctx.strokeText(data2[item].label, P1[0]-15, P1[1]+15);

//data[item].angle=currentAngle;

}
/*
draw2();
}

function draw2(){
*/
date=new Date();
var seconds=date.getSeconds();
var minutes=date.getMinutes();
var hours=date.getHours()+9;

if (hours>12){
hours=hours % 12;
}

var path = new Path2D();
path.moveTo(x,y);

var angles=0;
var anglem=0;
for (item in data2){
if(data2[item].label==seconds){
angles=data2[item].angle
}
}

for (item in data2){
if(data2[item].label==minutes){
anglem=data2[item].angle
}

}


for (item in data12){
if(data12[item].label==hours){
angleh=data12[item].angle
}
}

angleh=angleh+((1 / data12.length) * 2 * Math.PI)*(minutes/60)


//console.log(minutes);
var path = new Path2D();
path.moveTo(x,y);
var r=halfsize*0.4
P1=[r*Math.cos(angleh)+x,r*Math.sin(angleh)+y]
ctx.strokeStyle='#fff';
path.lineTo(P1[0],P1[1]);

ctx.stroke(path);

//console.log(hours);
//console.log(data)
//console.log(data2)
//console.log(seconds);
var path = new Path2D();
path.moveTo(x,y);
var r=0.55*halfsize
P1=[r*Math.cos(anglem)+x,r*Math.sin(anglem)+y]
path.lineTo(P1[0],P1[1]);
ctx.stroke(path);
ctx.strokeStyle='#fff';

var path = new Path2D();
path.moveTo(x,y);
var r=0.55*halfsize
P1=[r*Math.cos(angles)+x,r*Math.sin(angles)+y]
path.lineTo(P1[0],P1[1]);
ctx.strokeStyle='#f00';
ctx.stroke(path);
ctx.strokeStyle='#000';

}
