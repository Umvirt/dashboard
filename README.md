#UmVirt Dashboard

Developer: UmVirt Development Team

License: MIT

## About

Umvirt Dashboard is HTML5 Canvas based web-applications framework.

Main objective of this framework is developing non interactive dashboards user interfaces for transport, power plants & etc...

In simple words web-applications is web-pages which can be opened on screen by browser. As screen can be used small smartphones or big TV sets. Due to screen capture hardware & software dashboard screen can be streamed over Internet many miles away.

## Installation

Just open "index.html" with HTML5 compatible browser to run template.

## Configuration

Edit "index.html" as you wish.

Dashboard widgets ("widgets" variable) is list of widgets which displayed on dashboard.

Each widget have same mandatory parameters:

- id - widget identifier on dashboard.
- wtype - widget type
- size - widget size (diameter)
- position - widget center position. X & Y coordinates.

There are two types of widgets which currently supported

- gauge
- clock


